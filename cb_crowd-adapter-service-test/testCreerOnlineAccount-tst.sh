#/bin/sh
URL="http://$1:8280/services/CROWDAdapterService_v1"
echo "URL to request is $URL"
echo "file to use is $2"

SOAPACTION="urn:CreerOnlineAccount"

curl -X POST --header "SOAPAction: ${SOAPACTION}" --header "Content-Type: text/xml" --data @$2 ${URL}
