#!/bin/bash
# Bash Menu Script Example

PS3='Please choose your environment: '
options=("TST" "ACC" "PRD" "Quit")

select opt in "${options[@]}"
do
    case $opt in
        "TST")
	    return $opt
            ;;
        "ACC")
		return "ACC"
            ;;
        "PRD")
		return "PRD"
            ;;
        "Quit")
            break
            ;;
        *) echo invalid option;;
    esac
done
