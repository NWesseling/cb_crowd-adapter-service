<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:v1="http://adapters.consumentenbond.nl/crowd-adapter-service/IF/v1"
xmlns:v11="http://services.consumentenbond.nl/canoniek/formaatDefinitie/v1_0"
xmlns:v12="http://services.consumentenbond.nl/canoniek/dataTypeDefinitie/v1_0"
>
<xsl:output indent="yes"/>	
<xsl:param name="uitvoerResultaat" />	
<xsl:param name="type" />
<xsl:param name="code" />
<xsl:param name="omschrijving" />
<xsl:param name="specificatie" />
<xsl:param name="username" />
<xsl:param name="userinfo" />	
<xsl:template match="/">
<v1:CROWDSearchOnlineUserAccountAntwoord_IF>                
	<xsl:if test="$userinfo != ''"> 
	   <v11:SearchOnlineAccountAntwoord_IF>
	     <v11:userName><xsl:value-of select="$username"/></v11:userName>
	     <v11:UserInfoURL><xsl:value-of select="$userinfo"/></v11:UserInfoURL>
	   </v11:SearchOnlineAccountAntwoord_IF>
   </xsl:if>
  <v11:berichtAntwoorden> 
  <v11:berichtAntwoord>
    <v12:meldingen>
      <v12:melding>
        <v12:type><xsl:value-of select="$type"/></v12:type>
   		<v12:code><xsl:value-of select="$code"/></v12:code>
   		<v12:omschrijving><xsl:value-of select="$omschrijving"/></v12:omschrijving>
   		<v12:specificatie><xsl:value-of select="$specificatie"/></v12:specificatie>
  		</v12:melding>
	</v12:meldingen>
	<v12:uitvoerresultaat>
	<xsl:choose>
	<xsl:when test="$uitvoerResultaat = 'SUCCES'">SUCCES</xsl:when>
		<xsl:otherwise>FOUT</xsl:otherwise>
	</xsl:choose>
	</v12:uitvoerresultaat>
  </v11:berichtAntwoord>
  </v11:berichtAntwoorden>
 </v1:CROWDSearchOnlineUserAccountAntwoord_IF>
</xsl:template>
</xsl:stylesheet>