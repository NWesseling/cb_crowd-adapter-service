<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:v1="http://adapters.consumentenbond.nl/crowd-adapter-service/IF/v1"
xmlns:v11="http://services.consumentenbond.nl/canoniek/formaatDefinitie/v1_0"
xmlns:v12="http://services.consumentenbond.nl/canoniek/dataTypeDefinitie/v1_0"
>
<xsl:output indent="yes"/>	
<xsl:param name="uitvoerResultaat" />	
<xsl:param name="type" />
<xsl:param name="code" />
<xsl:param name="omschrijving" />
<xsl:param name="specificatie" />
<xsl:param name="userName" />
<xsl:param name="displayName" />
<xsl:param name="firstName" />
<xsl:param name="lastName" />
<xsl:param name="emailadres" />
<xsl:param name="password" />
<xsl:param name="active" />


<xsl:template match="/">
<v1:CROWDLeverOnlineAccountAntwoord_IF>
 <xsl:if test="$userName != ''"> 
	   <v11:OnlineAccount_IF>
	     <v11:userName><xsl:value-of select="$userName"/></v11:userName>
	     <v11:displayName><xsl:value-of select="$displayName"/></v11:displayName>
	     <v11:firstName><xsl:value-of select="$firstName"/></v11:firstName>
	     <v11:lastName><xsl:value-of select="$lastName"/></v11:lastName>
	     <v11:emailadres><xsl:value-of select="$emailadres"/></v11:emailadres>
	     <v11:password><xsl:value-of select="$password"/></v11:password>	    
	     <v11:active>
	      <xsl:choose>
	     	<xsl:when test="$active = 'false'">0</xsl:when>
	     	<xsl:otherwise>1</xsl:otherwise>
	     </xsl:choose>
	     </v11:active>
	   </v11:OnlineAccount_IF>
 </xsl:if>
	   <v11:berichtAntwoorden>
	  <v11:berichtAntwoord>
	    <v12:meldingen>
	      <v12:melding>
	        <v12:type><xsl:value-of select="$type"/></v12:type>
	   		<v12:code><xsl:value-of select="$code"/></v12:code>
	   		<v12:omschrijving><xsl:value-of select="$omschrijving"/></v12:omschrijving>
	   		<v12:specificatie><xsl:value-of select="$specificatie"/></v12:specificatie>
	  		</v12:melding>
		</v12:meldingen>
		<v12:uitvoerresultaat>
		<xsl:choose>
		<xsl:when test="$uitvoerResultaat = 'FOUT'">FOUT</xsl:when>
			<xsl:otherwise>SUCCES</xsl:otherwise>
		</xsl:choose>
		</v12:uitvoerresultaat>
	  </v11:berichtAntwoord>
	</v11:berichtAntwoorden>
   </v1:CROWDLeverOnlineAccountAntwoord_IF>
	</xsl:template>
</xsl:stylesheet>